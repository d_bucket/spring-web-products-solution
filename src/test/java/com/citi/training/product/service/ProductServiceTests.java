package com.citi.training.product.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.product.dao.ProductDao;
import com.citi.training.product.exceptions.ProductNotFoundException;
import com.citi.training.product.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {

	@Autowired
	private ProductService productService;

	@MockBean
	private ProductDao mockProductDao;

	@Test
	public void test_create() {
		int newID = 1;
		
		when(mockProductDao.create(any(Product.class))).thenReturn(newID);
		
		int createdId = productService.create(new Product(123, "Ham", 9.99));
		
		assertEquals(newID, createdId);
		
		verify(mockProductDao).create(any(Product.class));
	}
	@Test
	public void test_deleteById() {
		
		productService.deleteById(5);
		verify(mockProductDao).deleteById(5);
	}
	
	@Test
	public void findById_test() {

		int testID = 66;
		Product testProduct = new Product(testID, "Beans", 100.99);
		when(productService.findById(testID)).thenReturn(testProduct);
		Product returnedProduct = productService.findById(testID);

		verify(mockProductDao).findById(testID);
		assertEquals(testProduct,returnedProduct);
	}
	
	@Test
	public void findById_exception_test() {

		int id = 5;
		ProductNotFoundException testexception = new ProductNotFoundException("Product id: " + id + " was not found.");
		Object returnedexception = productService.findById(id);
		when(productService.findById(id)).thenReturn(null);
	}
	
	@Test
	public void findAll() {
		List<Product> testList = new ArrayList<Product>();
		testList.add(new Product(33,"LastTest",999));
		
		when(productService.findAll()).thenReturn(testList);
		List<Product> resultList = productService.findAll();
		
		verify(mockProductDao).findAll();
		assertEquals(testList,resultList);
	
	
	}

}
